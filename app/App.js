/**
 * Created by eatong on 17-3-13.
 */
import React, {Component}from 'react';
import {HashRouter, Route, Link} from 'react-router-dom';
import {Provider} from 'mobx-react';

import HomePage from './views/HomePage';
import TodoPage from './views/TodoPage';
import MainPage from './views/MainPage';
import BoardPage from './views/BoardPage';
import SettingPage from './views/SettingPage';

export default  class App extends Component {
  render() {
    return (
      <Provider>
        <HashRouter>
          <div>
            <Route exact path="/" component={MainPage}/>
            <Route exact path="/board" component={BoardPage}/>
            <Route exact path="/setting" component={SettingPage}/>
          </div>
        </HashRouter>
      </Provider>
    )
  }
}
