/**
 * Created by eatong on 17-3-13.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import {observer} from 'mobx-react';
import BookStore from '../stores/BookStore'
import { Button, TextInput,
  Text,
  View} from 'react-desktop/macOs';

@observer
class TodoPage extends React.Component {


  render() {
    const { books } = BookStore
    console.log('BookStore: ', BookStore)
    console.log('readBooks:', BookStore.readBooks)
    console.log('booksByErnestCline: ', BookStore.booksByAuthor('Ernest Cline'))
    return (
      <div className="todo-page">
        <Link to='/'>back to main page....</Link>

        <Text>Books</Text>
      
        {
          books.map((book, index) => (
              <View>
                <Text>{book.title}</Text>
                <Text>{book.author}</Text>
                <Text>Read: {book.read ? 'yes': 'no'}</Text>
              </View>
          ))
        }
      </div>
    );
  }
}
export default TodoPage;
