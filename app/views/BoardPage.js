/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import { Button, TextInput } from 'react-desktop/macOs';
import BoardToolbar from './BoardToolbar';
import BoardPanel from './BoardPanel';

@observer 
class BoardPage extends Component {
  constructor(props) {
    console.log('constructor...');
    super(props);
    this.state = {};
  }

  componentWillMount() {
  }

  componentDidMount() {

  }

  render() {
    return (
      <div>
      <BoardToolbar/>
      <BoardPanel/>
      </div>
    );
  }
}

export default BoardPage;
