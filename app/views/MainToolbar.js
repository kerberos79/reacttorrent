/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import TorrentItem from '../stores/TorrentItem';
import GlobalStore from '../stores/GlobalStore';
import { ButtonToolbar, Button, Glyphicon, Modal, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
var fs = require('fs');
const {dialog} = require('electron').remote;
@observer 
class MainToolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRemoveModal: false
    };
  }
  
  handleClickAdd(e) {
    console.log('handleClickAdd');
    dialog.showOpenDialog({properties: ['openFile', 'multiSelections']},
    (fileNames) => {
      // fileNames is an array that contains all the selected
      if(fileNames === undefined){
          console.log("No file selected");
          return;
      }
  
      console.log("The file content is : " + fileNames[0]);
      TorrentItem.addTorrentFile(fileNames[0]);
      /*
      fs.readFile(filepath, 'utf-8', (err, data) => {
          if(err){
              alert("An error ocurred reading the file :" + err.message);
              return;
          }
  
          // Change how to handle the file content
          console.log("The file content is : " + data);
      });
      */
  });
  }
  handleClickAddUrl(e) {
    console.log('handleClickAddUrl');
  }
  handleClickStart(e) {
    console.log('handleClickStart');
    TorrentItem.resumeDownloading();
  }
  handleClickStop(e) {
    console.log('handleClickStop');
    TorrentItem.pauseDownloading();
  }
  handleClickRemove(e) {
    console.log('handleClickRemove');
    TorrentItem.removeSelectedItem();
  }

  handleCloseRemoveModal() {
    this.setState({ showRemoveModal: false });
  }

  handleRemoveFromList() {
    this.setState({ showRemoveModal: false });
  }
  handleRemoveAll() {
    this.setState({ showRemoveModal: false });
    TorrentItem.removeDownloading();
  }

  handleShowRemoveModal() {
    this.setState({ showRemoveModal: true });
  }
  render() {
    const styleToolbar = {
      width: '100%',
      height: 55,
      padding: 10
    }
    return (
      <div>
      <ButtonToolbar style={styleToolbar}>
        <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
          <ToggleButton
            value={1}>
            <Glyphicon glyph="home" /> Home
          </ToggleButton>
          <ToggleButton
            value={2}>
            <Link to='/board'><Glyphicon glyph="search" /> Search</Link>
          </ToggleButton>
          <ToggleButton
            value={3}>
            <Link to='/setting'><Glyphicon glyph="cog" /> Setting</Link>
          </ToggleButton>
        </ToggleButtonGroup>
        <Button
          onClick={this.handleClickAdd.bind(this)}>
          <Glyphicon glyph="plus" /> Add
        </Button>
        <Button
          onClick={this.handleClickAddUrl.bind(this)}>
          <Glyphicon glyph="plus" /> Add Url
        </Button>
        <Button
          onClick={this.handleClickStart.bind(this)}
          disabled={!TorrentItem.hasSelected()}>
          <Glyphicon glyph="play" /> Start
        </Button>
        <Button
          onClick={this.handleClickStop.bind(this)}
          disabled={!TorrentItem.hasSelected()}>
          <Glyphicon glyph="stop" /> Stop
        </Button>
        <Button
          onClick={this.handleShowRemoveModal.bind(this)}
          disabled={!TorrentItem.hasSelected()}>
          <Glyphicon glyph="trash" /> Remove
        </Button>
      </ButtonToolbar>

    <Modal show={this.state.showRemoveModal} onHide={this.handleCloseRemoveModal.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Alert</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Remove selected torrrents</h4>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.handleCloseRemoveModal.bind(this)}>Close</Button>
        <Button bsStyle="primary" onClick={this.handleRemoveAll.bind(this)}>Remove Files</Button>
      </Modal.Footer>
    </Modal>
      </div>
    );
  }
}

export default MainToolbar;
