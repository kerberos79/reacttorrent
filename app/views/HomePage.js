/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import BookStore from '../stores/BookStore'
import { Button, TextInput } from 'react-desktop/macOs';

const initialState = {
  title: '',
  author: '',
  read: false
}
let renderCount = 0;
@observer 
class HomePage extends Component {
  constructor(props) {
    console.log('constructor...');
    super(props);
    this.state = {};
  }

  componentWillMount() {
  }

  componentDidMount() {

  }

  state = initialState
  onChangeText = (key, value) => {
    console.log('onChangeText '+key+' : '+value);
    this.setState({
      [key]: value
    })
  }

  addBook() {
    if (!this.state.title || !this.state.author) return
    BookStore.addBook(this.state)
    this.setState(initialState)
    console.log('addBook '+this.state.title+' : '+this.state.author);
  }

  render() {
    renderCount++;
    return (
      <div className="">
        This is home page...
        <br/>
        <Link to='/mainlist'>todo list</Link>


        <TextInput
          label="title"
          placeholder="title"
          defaultValue=""
          onChange={titleInput => this.onChangeText('title', titleInput.target.value)}
        />
        <TextInput
          label="author"
          placeholder="author"
          defaultValue=""
          onChange={authorInput => this.onChangeText('author',  authorInput.target.value)}
        />
        <Button color="blue" onClick={this.addBook.bind(this)}>
          Press me!
        </Button>
      </div>
    );
  }
}

HomePage.propTypes = {};
export default HomePage;
