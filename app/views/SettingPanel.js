/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import { Tabs, Tab } from 'react-bootstrap';

import SettingGeneralPanel from './SettingGeneralPanel';
import SettingBandwidthPanel from './SettingBandwidthPanel';
import SettingDirectoriesPanel from './SettingDirectoriesPanel';
import SettingNetworkPanel from './SettingNetworkPanel';
@observer 
class SettingPanel extends Component {
  constructor(props) {
    console.log('constructor...');
    super(props);
    this.state = {
      key: 1
    };
  }

  handleSelect(key) {
    this.setState({ key });
  }

  render() {
    return (
      <Tabs
        activeKey={this.state.key}
        onSelect={this.handleSelect.bind(this)}
        id="controlled-tab-example"
      >
        <Tab eventKey={1} title="General">
        <SettingGeneralPanel/>
        </Tab>
        <Tab eventKey={2} title="Bandwidth">
        <SettingBandwidthPanel/>
        </Tab>
        <Tab eventKey={3} title="Directories">
        <SettingDirectoriesPanel/>
        </Tab>
        <Tab eventKey={4} title="Network">
        <SettingNetworkPanel/>
        </Tab>
      </Tabs>
    );
  }
}

export default SettingPanel;
