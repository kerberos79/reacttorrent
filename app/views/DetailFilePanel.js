/**
 * Created by eatong on 17-3-13.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import {observer} from 'mobx-react';
import TorrentItem from '../stores/TorrentItem';
import FileInfoItem from '../stores/FileInfoItem';

import ReactDataGrid from 'react-data-grid';
import DetailFilePanelRowRenderer from '../components/DetailFilePanelRowRenderer';
import PercentCompleteFormatter from '../components/PercentCompleteFormatter';
import ByteStringFormatter from '../components/ByteStringFormatter';
import SpeedFormatter from '../components/SpeedFormatter';

@observer
class DetailFilePanel extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      columns: [
        { key: 'name', name: 'Name', width:600, resizable: true, sortable: true },
        { key: 'path', name: 'Path', width:100, resizable: true, sortable: true },
        { key: 'length', name: 'Size', width:100, resizable: true, sortable: true, formatter: ByteStringFormatter },
        { key: 'progress', name: 'Progress %', width:140, resizable: true, sortable: false, formatter: PercentCompleteFormatter},
        { key: 'downloaded', name: 'Downloaded', width:120, resizable: true, sortable: true, formatter: ByteStringFormatter },
      ],
      selectedRows: [],
      isShiftKeyPressed:false,
      selectedRowInx:-1
    };

    const refreshTimer = setInterval(() => {
      FileInfoItem.refresh();
    }, 3000);
  }

  handleGridSort = (sortColumn, sortDirection) => {
    const comparer = (a, b) => {
      if (sortDirection === 'ASC') {
        return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
      } else if (sortDirection === 'DESC') {
        return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
      }
    };
    FileInfoItem.sort(comparer);
  };
  rowGetter = (i) => {
    return FileInfoItem.items[i];
  };

  onRowClick = (rowIdx, row) => {
    if(rowIdx<0)
      return;
    const { isShiftKeyPressed, selectedRowInx } = this.state;
    if(isShiftKeyPressed) {
      if(selectedRowInx==-1 || rowIdx==selectedRowInx) {
        FileInfoItem.items[rowIdx].toggleSelected();
      }
      else if(rowIdx>selectedRowInx){
        for(let i=selectedRowInx+1;i<=rowIdx;i++)
          FileInfoItem.items[i].toggleSelected();
        this.setState({ selectedRowInx : -1 });
      }
      else{
        for(let i=rowIdx;i<selectedRowInx;i++)
          FileInfoItem.items[i].toggleSelected();
        this.setState({ selectedRowInx : -1 });
      }
    }
    else {
      FileInfoItem.deselectedItems();

      if(this.state.selectedRowInx == rowIdx) {
        this.setState({ selectedRowInx : -1 });
        FileInfoItem.setHasSelected(false);
      }
      else {
        this.setState({ selectedRowInx : rowIdx });
        FileInfoItem.items[rowIdx].toggleSelected();
        FileInfoItem.setHasSelected(true);
      }
    }
  };

  onKeyDown = (e) => {
    console.log('onKeyDown : '+e.keyCode);
    if (e.keyCode === 16) {
      this.setState({ isShiftKeyPressed : true });
    }
  };
  onKeyUp = (e) => {
    console.log('onKeyUp : '+e.keyCode);
    this.setState({ isShiftKeyPressed : false });
  };

  getCellActions(column, row) {
    if (column.key === 'name') {
      return [
        {
          icon: 'glyphicon glyphicon-play-circle',
          callback: () => { alert('playing'); }
        },
      ];
    }
  }
  render() {
    const { columns } = this.state;
    return  (
      <ReactDataGrid
        onGridSort={this.handleGridSort}
        rowSelection={{
          showCheckbox: false,
          selectBy: {
            isSelectedKey: 'isSelected'
          }
        }}
        enableCellSelect={false}
        onRowClick={this.onRowClick}
        onGridKeyDown={this.onKeyDown}
        onGridKeyUp={this.onKeyUp}
        columns={columns}
        rowGetter={this.rowGetter}
        rowsCount={FileInfoItem.items.length}
        getCellActions={this.getCellActions}
        minHeight={500}
        rowRenderer={DetailFilePanelRowRenderer} />
      );
  }
}
export default DetailFilePanel;
