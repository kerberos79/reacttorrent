/**
 * Created by eatong on 17-3-13.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import {observer} from 'mobx-react';
import TorrentItem from '../stores/TorrentItem';

import ReactDataGrid from 'react-data-grid';
import MainListRowRenderer from '../components/MainListRowRenderer';
import PercentCompleteFormatter from '../components/PercentCompleteFormatter';
import ByteStringFormatter from '../components/ByteStringFormatter';
import SpeedFormatter from '../components/SpeedFormatter';

@observer
class MainList extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      columns: [
        { key: 'name', name: 'Name', width:600, resizable: true, sortable: true },
        { key: 'received', name: 'Received', width:100, resizable: true, sortable: true, formatter: ByteStringFormatter },
        { key: 'downloaded', name: 'Downloaded', width:100, resizable: true, sortable: true, formatter: ByteStringFormatter },
        { key: 'downloadSpeed', name: '↓ Speed', width:100, resizable: true, sortable: true, formatter: SpeedFormatter },
        { key: 'uploadSpeed', name: '↑ Speed', width:100, resizable: true, sortable: true, formatter: SpeedFormatter },
        { key: 'progress', name: 'Progress %', width:140, resizable: true, sortable: false, formatter: PercentCompleteFormatter},
        { key: 'timeRemaining', name: 'Remaining', width:120, resizable: true, sortable: true },
        { key: 'status', name: 'Status', width:120, resizable: true, sortable: true },
        { key: 'ratio', name: 'Ratio', width:120, resizable: true, sortable: true },
        { key: 'numPeers', name: 'NumPeers', width:120, resizable: true, sortable: true },
      ],
      selectedRows: [],
      isShiftKeyPressed:false,
      selectedRowInx:-1
    };

    if(!TorrentItem.initialized) {
      TorrentItem.initialize();
      
      const refreshTimer = setInterval(() => {
        TorrentItem.refresh();
      }, 3000);
    }
  }

  handleGridSort = (sortColumn, sortDirection) => {
    const comparer = (a, b) => {
      if (sortDirection === 'ASC') {
        return (a[sortColumn] > b[sortColumn]) ? 1 : -1;
      } else if (sortDirection === 'DESC') {
        return (a[sortColumn] < b[sortColumn]) ? 1 : -1;
      }
    };
    TorrentItem.sort(comparer);
  };
  rowGetter = (i) => {
    return TorrentItem.items[i];
  };

  onRowClick = (rowIdx, row) => {
    if(rowIdx<0)
      return;
    const { isShiftKeyPressed, selectedRowInx } = this.state;
    if(isShiftKeyPressed) {
      if(selectedRowInx==-1 || rowIdx==selectedRowInx) {
        TorrentItem.items[rowIdx].toggleSelected();
      }
      else if(rowIdx>selectedRowInx){
        for(let i=selectedRowInx+1;i<=rowIdx;i++)
          TorrentItem.items[i].toggleSelected();
        this.setState({ selectedRowInx : -1 });
      }
      else{
        for(let i=rowIdx;i<selectedRowInx;i++)
          TorrentItem.items[i].toggleSelected();
        this.setState({ selectedRowInx : -1 });
      }
    }
    else {
      TorrentItem.deselectedItems();

      if(this.state.selectedRowInx == rowIdx) {
        this.setState({ selectedRowInx : -1 });
        TorrentItem.setHasSelected(false);
        TorrentItem.removeFileInfo();
      }
      else {
        this.setState({ selectedRowInx : rowIdx });
        TorrentItem.items[rowIdx].toggleSelected();
        TorrentItem.setHasSelected(true);
        TorrentItem.setSelectedTorrentFileInfo(TorrentItem.items[rowIdx]);
      }
    }
  };

  onKeyDown = (e) => {
    console.log('onKeyDown : '+e.keyCode);
    if (e.keyCode === 16) {
      this.setState({ isShiftKeyPressed : true });
    }
  };
  onKeyUp = (e) => {
    console.log('onKeyUp : '+e.keyCode);
    this.setState({ isShiftKeyPressed : false });
  };

  getCellActions(column, row) {
    if (column.key === 'name') {
      return [
        {
          icon: 'glyphicon glyphicon-folder-open',
          callback: () => { alert('Deleting'); }
        },
      ];
    }
  }
  render() {
    const stylList = {
      width: '100%',
      height: 400
    }
    const { columns } = this.state;
    const { items } = TorrentItem;
    return  (
      <ReactDataGrid
        style={stylList}
        onGridSort={this.handleGridSort}
        rowSelection={{
          showCheckbox: false,
          selectBy: {
            isSelectedKey: 'isSelected'
          }
        }}
        
        enableCellSelect={false}
        onRowClick={this.onRowClick}
        onGridKeyDown={this.onKeyDown}
        onGridKeyUp={this.onKeyUp}
        columns={columns}
        rowGetter={this.rowGetter}
        rowsCount={items.length}
        getCellActions={this.getCellActions}
        minHeight={this.props.height}
        rowRenderer={MainListRowRenderer} />
      );
  }
}
export default MainList;
