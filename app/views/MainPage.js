/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import SplitterLayout from 'react-splitter-layout';
import MainToolbar from './MainToolbar';
import MainList from './MainList';
import DetailPanel from './DetailPanel';
import ReactResizeDetector from 'react-resize-detector';
@observer 
class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height:400,
      secondPanelHeight:400,
    };
  }

  onResize = () => { 
    this.setState({ height: window.innerHeight - this.state.secondPanelHeight - 4 }) 
  }
  onSecondaryPaneSizeChange = (e) => {
    this.setState({ 
      secondPanelHeight: e ,
      height : (window.innerHeight - e - 4)
    }) 
  };
  render() {
    const styleMain = {
      width: '100%',
      height: 400
    }

    return (
      <div>
        <MainToolbar/>
        <SplitterLayout 
          vertical
          onSecondaryPaneSizeChange = {this.onSecondaryPaneSizeChange.bind(this)}>
          <MainList style={styleMain} height={this.state.height}/>
          <DetailPanel/>
        </SplitterLayout>
        <ReactResizeDetector handleWidth handleHeight onResize={this.onResize.bind(this)} />
      </div>
    );
  }
}

export default MainPage;
