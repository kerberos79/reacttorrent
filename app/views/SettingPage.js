/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import { Button, TextInput } from 'react-desktop/macOs';
import SettingToolbar from './SettingToolbar';
import SettingPanel from './SettingPanel';

@observer 
class SettingPage extends Component {
  constructor(props) {
    console.log('constructor...');
    super(props);
    this.state = {};
  }

  componentWillMount() {
  }

  componentDidMount() {

  }

  render() {
    return (
      <div>
      <SettingToolbar/>
      <SettingPanel/>
      </div>
    );
  }
}

export default SettingPage;
