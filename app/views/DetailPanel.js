/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import { Tabs, Tab } from 'react-bootstrap';

import DetailFilePanel from './DetailFilePanel';
import FileInfoItem from '../stores/FileInfoItem';
@observer 
class DetailPanel extends Component {
  constructor(props) {
    console.log('constructor...');
    super(props);
    this.state = {
      key: 1
    };
  }

  handleSelect(key) {
    this.setState({ key });
  }

  render() {
    return (
      <Tabs
        activeKey={this.state.key}
        onSelect={this.handleSelect.bind(this)}
        id="controlled-tab-example"
      >
        <Tab eventKey={1} title="Files">
        <DetailFilePanel/>
        </Tab>
        <Tab eventKey={2} title="Status">
          Tab 2 content
        </Tab>
        <Tab eventKey={3} title="Tab 3">
          Tab 3 content
        </Tab>
      </Tabs>
    );
  }
}

export default DetailPanel;
