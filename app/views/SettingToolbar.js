/**
 * Created by eatong on 17-3-13.
 */
import React, {PropTypes, Component} from 'react';
import {Link} from 'react-router-dom';
import { decorate, observable } from 'mobx';
import {observer} from 'mobx-react';
import GlobalStore from '../stores/GlobalStore';
import { ButtonToolbar, Button, Glyphicon, Modal, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';

@observer 
class SettingToolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRemoveModal: false
    };
  }
  render() {
    const styleToolbar = {
      width: '100%',
      height: 55,
      padding: 10
    }
    return (
      <div>
      <ButtonToolbar style={styleToolbar}>
        <ToggleButtonGroup type="radio" name="options" defaultValue={3}>
          <ToggleButton
            value={1}>
            <Link to='/'><Glyphicon glyph="home" /> Home</Link>
          </ToggleButton>
          <ToggleButton
            value={2}>
            <Link to='/board'><Glyphicon glyph="search" /> Search</Link>
          </ToggleButton>
          <ToggleButton
            value={3}>
            <Glyphicon glyph="cog" /> Setting
          </ToggleButton>
        </ToggleButtonGroup>
      </ButtonToolbar>
      </div>
    );
  }
}

export default SettingToolbar;
