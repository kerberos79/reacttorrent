import { types, destroy } from "mobx-state-tree"
import WebTorrent from 'webtorrent';
import FileInfoItem from '../stores/FileInfoItem';

var client;

const Torrent = types.model('Torrent', {
    name: types.string,
    torrentId: types.string,
    hash: types.string,
    received: types.number,
    downloaded: types.number,
    downloadSpeed: types.number,
    uploadSpeed: types.number,
    progress: types.number,
    ratio: types.number,
    numPeers: types.number,
    timeRemaining: types.number,
    status: types.string,
    isCompleted: types.boolean,
    isSelected: types.boolean
}).actions(self => ({
    toggleRead() {
        self.read = !self.read
    },
    incrementComplete() {
      console.log('incrementComplete '+self.complete);
        self.complete++;
    },
    toggleSelected() {
      console.log('toggleSelected '+self.complete);
        self.isSelected = !self.isSelected;
    },
    setSelected(selected) {
        self.isSelected = selected;
    },
    setDownloadStatus(torrent) {
      self.received = torrent.received
      self.downloaded = torrent.downloaded
      self.downloadSpeed = torrent.downloadSpeed
      self.progress = torrent.progress;
      self.numPeers = torrent.numPeers;
      self.ratio = torrent.ratio;
      self.timeRemaining = torrent.timeRemaining;

      if(torrent.infoHash===self.hash) {
        FileInfoItem.setDownloadStatus(torrent); 
      }
    },
    setStatus(value) {
      self.status = value;
    }
}))

const TorrentItem = types.model({
    items: types.array(Torrent),
    selectedItem:false,
    initialized:false,
    updated:false
  })
  .views(self => ({
    getTorrent(identifier) {
      console.log('getTorrent '+identifier);
      return self.items.find(item => item.id === identifier) || null
    },
    get readTorrent() {
      return self.items.filter(item => item.read)
    },
    torrentByHash(hash) {
      return self.items.find(item => item.hash === hash)
    },
    torrentSelected() {
      return self.items.filter(item => item.isSelected === true)
    },
    torrentCompleted() {
      return self.items.filter(item => item.isCompleted === true)
    },
    hasSelected() {
      return self.selectedItem
    }
  })

  )
  .actions(self => ({
    addItem(item) {
      self.items.push(item)
    },
    remove(item) {
      destroy(item)
    },
    sort(compare) {
      self.items.replace(self.items.sort(compare))
    },
    deselectedItems() {
      let results = self.items.filter(item => item.isSelected);
      for(let i=0;i<results.length;i++) {
        results[i].toggleSelected();
      }
    },
    removeSelectedItem() {
      let results = self.items.filter(item => item.isSelected);
      for(let i=0;i<results.length;i++) {
        destroy(results[i]);
      }
    },
    setHasSelected(selected) {
      self.selectedItem = selected
    },
    initialize() {
      self.initialized = true;
      client = new WebTorrent();
      console.log('initialize : '+client);
    },
    refresh() {
      self.updated=!self.updated
    },
    pauseDownloading() {
      let results = self.items.filter(item => item.isSelected);
      for(let i=0;i<results.length;i++) {
        results[i].setStatus('Stopped');
        let torrents = client.torrents.filter(torrent => torrent.infoHash === results[i].hash);
        console.log('pauseDownloading '+torrents.length);
        if(torrents==null)
          continue;
        for(let i=0;i<torrents.length;i++) {
          torrents[i].pause();
        console.log('pauseDownloading '+torrents[i].name);
        }
      }
    },
    resumeDownloading() {
      let results = self.items.filter(item => item.isSelected);
      for(let i=0;i<results.length;i++) {
        results[i].setStatus('Downloading');
        let torrents = client.torrents.filter(torrent => torrent.infoHash === results[i].hash);
        console.log('resumeDownloading '+torrents.length);
        if(torrents==null)
          continue;
        for(let i=0;i<torrents.length;i++) {
          torrents[i].resume();
          console.log('resumeDownloading '+torrents[i].name);
        }
      }
    },
    removeDownloading() {
      let results = self.items.filter(item => item.isSelected);
      for(let i=0;i<results.length;i++) {
        client.remove(results[i].torrentId);
        this.remove(results[i]);
      }
    },
    addTorrentFile(path) {
      if(client==null){
        console.log('addTorrentFile client is null');
        return;
      }
      
      console.log('addTorrentFile client : '+path);
      client.add(path, {
              //announce: [String],        // Torrent trackers to use (added to list in .torrent or magnet uri)
              //getAnnounceOpts: Function, // Custom callback to allow sending extra parameters to the tracker
              maxWebConns: 10,       // Max number of simultaneous connections per web seed [default=4]
              path: '/Users/keros/torrent/',              // Folder to download files to (default=`/tmp/webtorrent/`)
              //store: Function            // Custom chunk store (must follow [abstract-chunk-store](https://www.npmjs.com/package/abstract-chunk-store) API)
            }, function (torrent) {
              console.log('ontorrent : ' + torrent.name);
              let item = Torrent.create({
                name: torrent.name,
                torrentId:path,
                hash: torrent.infoHash,
                received: torrent.received,
                downloaded: torrent.downloaded,
                downloadSpeed: torrent.downloadSpeed,
                uploadSpeed: torrent.uploadSpeed,
                progress: torrent.progress,
                ratio: torrent.ratio,
                numPeers: torrent.numPeers,
                timeRemaining:torrent.timeRemaining,
                status:"Downloading",
                isCompleted: false,
                isSelected: false
              })
              
              console.log('client add' + item.name);
              self.addItem(item);

              torrent.on('download', function (bytes) {
                let torrentItem = self.torrentByHash(torrent.infoHash)
                if(torrentItem==null){
                  console.log('not found torrent : '+torrent.infoHash);
                  return;
                }
                torrentItem.setDownloadStatus(torrent)
              })
              .on('infoHash', function () {
                console.log('torrent infoHash: ' + torrent.infoHash)
              })
              .on('error', function (err) {
                console.log('torrent error : ' + err)
              })
            })
            .on('error', function (err) {
              console.log(err);
            })
    },
    setSelectedTorrentFileInfo(currenttorrent) {
      
      let torrents = client.torrents.filter(torrent => torrent.infoHash === currenttorrent.hash);
      if(torrents==null) {
        console.log('setSelectedTorrentFileInfo torrents is null');
        return;
      }
      console.log('setSelectedTorrentFileInfo '+torrents.length);
      FileInfoItem.setTorrentFiles(torrents[0].files);
    },
    removeFileInfo() {
      FileInfoItem.clear();
    }


  }))
  .create({
    items:[],
  })

export default TorrentItem
