import { types, destroy } from "mobx-state-tree"

const GlobalStore = types.model({
    selectedListIndex: types.number,
  })
  .views(self => ({
    get listIndex() {
      return self.selectedListIndex;
    },
  })

  )
  .actions(self => ({
    setSelectedListIndex(index) {
      self.selectedListIndex = index;
    }
  }))
  .create({
    selectedListIndex:-1,
  })

export default GlobalStore
