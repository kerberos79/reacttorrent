import { types, destroy } from "mobx-state-tree"


const FileInfo = types.model('FileInfo', {
    name: types.string,
    path: types.string,
    length: types.number,
    downloaded: types.number,
    progress: types.number,
    isSelected: types.boolean
}).actions(self => ({
    toggleSelected() {
      console.log('toggleSelected '+self.complete);
        self.isSelected = !self.isSelected;
    },
    setSelected(selected) {
        self.isSelected = selected;
    }
}))

const FileInfoItem = types.model({
    items: types.array(FileInfo),
    selectedItem:false,
    updated:false
  })
  .views(self => ({
    getTorrent(identifier) {
      console.log('getTorrent '+identifier);
      return self.items.find(item => item.id === identifier) || null
    },
    get readTorrent() {
      return self.items.filter(item => item.read)
    },
    torrentSelected() {
      return self.items.filter(item => item.isSelected === true)
    },
    torrentCompleted() {
      return self.items.filter(item => item.isCompleted === true)
    },
    hasSelected() {
      return self.selectedItem
    }
  })
    )

  .actions(self => ({
    addItem(item) {
      self.items.push(item)
    },
    remove(item) {
      destroy(item)
    },
    clear() {
      self.items.clear()
    },
    sort(compare) {
      self.items.replace(self.items.sort(compare))
    },
    deselectedItems() {
      let results = self.items.filter(item => item.isSelected);
      for(let i=0;i<results.length;i++) {
        results[i].toggleSelected();
      }
    },
    removeSelectedItem() {
      let results = self.items.filter(item => item.isSelected);
      for(let i=0;i<results.length;i++) {
        destroy(results[i]);
      }
    },
    refresh() {
      self.updated=!self.updated
    },
    setHasSelected(selected) {
      self.selectedItem = selected
    },
    setTorrentFiles(files) {
      self.items.clear();
      
      for(let i=0;i<files.length;i++) {
        let file = files[i];
        let item = FileInfo.create({
          name: file.name,
          path: file.path,
          length: file.length,
          downloaded: file.downloaded,
          progress: file.progress,
          isSelected: false
        })
        self.addItem(item);
      }
    },
    setDownloadStatus(torrent) {
      for(let i=0;i<torrent.files.length;i++) {
        let file = torrent.files[i];
        
        for(let j=0;j<self.items.length;j++) {
          if(file.name === self.items[j].name) {
            self.items[j].progress = file.progress;
            self.items[j].downloaded = file.downloaded;
            break;
          }
        }
      }
    }

  }))
.create({
  items:[],
})

export default FileInfoItem