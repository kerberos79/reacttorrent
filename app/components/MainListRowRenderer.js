
import React from 'react';
import PropTypes from 'prop-types';
import ReactDataGrid from 'react-data-grid';
import { Row } from 'react-data-grid';
import {observer} from 'mobx-react';
import TorrentItem from '../stores/TorrentItem';

@observer
class MainListRowRenderer extends React.Component {
    static propTypes = {
      idx: PropTypes.number.isRequired
    };
  
    setScrollLeft = (scrollBy) => {
        this.row.setScrollLeft(scrollBy);
      };
    render() {
        const { updated } = TorrentItem;
        return (<Row ref={ node => this.row = node } {...this.props}  forceUpdate={true}/>);
    }
  }
  export default MainListRowRenderer;