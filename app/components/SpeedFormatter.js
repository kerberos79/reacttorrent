import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

class SpeedFormatter extends React.Component {
    static propTypes = {
      value: PropTypes.number.isRequired
    };
    
    bytesToSize(bytes,decimalPoint) {
      if(bytes == 0) return '0 Bytes';
      var k = 1000,
          dm = decimalPoint || 2,
          sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
          i = Math.floor(Math.log(bytes) / Math.log(k));
      return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
     };
    render() {
      return (
        <div>{this.bytesToSize(this.props.value, 2)}/s</div>
        );
    }
  }
  export default SpeedFormatter;