
import React from 'react';
import PropTypes from 'prop-types';
import ReactDataGrid from 'react-data-grid';
import { Row } from 'react-data-grid';
import {observer} from 'mobx-react';
import FileInfoItem from '../stores/FileInfoItem';

@observer
class DetailFilePanelRowRenderer extends React.Component {
    static propTypes = {
      idx: PropTypes.number.isRequired
    };
  
    setScrollLeft = (scrollBy) => {
        this.row.setScrollLeft(scrollBy);
      };
    render() {
        const { updated } = FileInfoItem;
        return (<Row ref={ node => this.row = node } {...this.props}  forceUpdate={true}/>);
    }
  }
  export default DetailFilePanelRowRenderer;