
import React from 'react';
import PropTypes from 'prop-types';
import { ProgressBar, Button } from 'react-bootstrap';

class PercentCompleteFormatter extends React.Component {
    static propTypes = {
      value: PropTypes.number.isRequired
    };
  
    render() {
      const styleBar = {
        margin: '5px',
      }
      const percentComplete = Math.floor(this.props.value*100.0);
      return (
        <ProgressBar style={styleBar} bsStyle="success" now={percentComplete} label={`${percentComplete}%`} />
        );
    }
  }
  export default PercentCompleteFormatter;